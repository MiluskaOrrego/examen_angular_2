import { Component, OnInit } from '@angular/core';
import { HttpClientService } from '../service.service';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss']
})
export class DepartmentComponent implements OnInit {

constructor(private httpService: HttpClientService) { }

ngOnInit ():void{
  this.httpService.getObjet('https://geo.api.gouv.fr/departements')
  .subscribe(response) => {
    console.log(response);
  }
}
}
