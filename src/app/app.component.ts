import { Component } from '@angular/core';
import { HttpClientService } from './service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'examAngularMiluska';
}

