import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-region',
  templateUrl: './region.component.html',
  styleUrls: ['./region.component.scss']
})
export class RegionComponent implements OnInit {
  constructor(private httpService: HttpClientService){
    }
}

ngOnInit (): void{
  this.httpService.getObjet('https://geo.api.gouv.fr/regions')
  .subscribe(response) => {
    console.log(response);
  }
}


